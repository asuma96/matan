@extends('layouts.app')
@section('content')
    <div id="calc_list">
        <h4>Выберите рассчет для вывода (отсортировано по дате):</h4>
        <ul>
            <?php if (isset($rolls)): ?>
            <?php foreach ($rolls as $item): ?>
            <li style="width: 230px"> <button data-id="<?=$item["id"]?>" class="show_calc btn"><?=$item["name"]?> - N: <?=$item["n"]?>, M: <?=$item["m"]?></button> </li>
            <br>
            <?php endforeach; ?>
            <?php endif; ?>
                {{$rolls->links()}}
        </ul>
    </div>
    <div class="container">


    <div id="roll" class="hidden">
        <a href="/disp"><button class="btn btn-info" style="margin-left: 5px">Назад к выбору рассчета</button></a>
        <br> <br>
        <div class="roll_name"></div>
        <div style="display: grid">
        <table class = "table">
            <thead>
            <tr>
                <th>M</th>
                <th>N</th>
                <th>C, CX</th>
                <th>DN</th>
                <th>DV</th>
                <th>BN</th>
                <th>BV</th>
                <th>Матрица A</th>
                <th>План</th>
            </tr>
            </thead>
            <tr>
                <th id = "m"></th>
                <th id = "n"></th>
                <th id = "c_cx"></th>
                <th id = "dn"></th>
                <th id = "dv"></th>
                <th id = "bn"></th>
                <th id = "bv"></th>
                <th id = "a">
                    <table>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </th>
                <th id="plan_b">

                </th>
            </tr>
        </table>
        <button class="btn btn-info sound_data">Решить</button>
    </div>
    </div>
    </div>
@endsection