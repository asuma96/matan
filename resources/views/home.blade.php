@extends('layouts.app')

@section('content')

    <div class="input_warp">
        {{--<div class="panel panel-danger hidden">

                    <div class="panel-body">Введенные значения не входят в диапазон от 1 до 25</div>

        </div>--}}
        <form   action="{{ url('/next') }}" role="form" method="POST">
            <div class="form-group col-md-6 col-offset-6">
                Количество временных интервалов

                <input type="number" autofocus required class = "input-inter input-ko home-input form-control input-small" name = "inter" id="inter">
                <br>
                Количество решаемых задач

                <input type="number" required class = "input-iter input-ko home-input form-control input-small" name = "iter" id="iter">
                <br>
                <button type="submit" class = "start button-home  btn btn-success">Начать</button>
            </div>
        </form>
    </div>
@endsection
