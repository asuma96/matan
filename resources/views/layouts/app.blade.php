<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                            <a class="new navbar-brand" href="{{ url('/') }}">Генератор 2.0</a>
                            <li><a href="{{ url('/login') }}">Login</a>
                            <li><a href="{{ url('/register') }}">Register</a>
                                @elseif(Auth::user()->roleId == 1 )

                                <div class=" new navbar-header">

                                    <a class="navbar-brand"  href="/home">Генератор 2.0</a>
                                    </div>
                            <li class="new navbar-header">
                                            <a class="logout" href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span>Выход</span>
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        <a class="navbar-brand "  >Администратор</a>
                    @else

                        <a class="navbar-brand "  >Генератор 2.0</a>
                                <div class="navbar-header">
                                </div>
                        <li class="dropdown">
                            <a class="logout" href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <span>Выход</span>
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @if (Auth::user()->roleId == 2)
                        <a class="navbar-brand "  >Диспетчер</a>
                    @elseif(Auth::user()->roleId == 3)
                            <a class="navbar-brand "  >Руководитель</a>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
{{--<script src="/js/common.js"></script>--}}
<script src="/js/matrix.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
