<?php


use yii\helpers\Html;
use app\assets\AppAsset;
use yii\web\Session;


$session = new Session();
$status = $session['status'];


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Генератор 1.0</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a >Курсовая работа Урусов А.О. ИС-4</a></li>
                <?php if ($status == "2"): ?>
                    <li><a href="/output">Просмотр результатов</a></li>
                <?php endif;?>
                <?php if ($status == "3"): ?>
                    <li><a href="/input">Ввод данных</a></li>
                <?php endif;?>
                <?php if ($status != ''): ?>
                <li><a href="/logoff">Выйти</a></li>
                <?php endif; ?>

            </ul>
        </div>
    </nav>
    <div class="container">
        <?= $content ?>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
