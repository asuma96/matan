@extends('layouts.app')

@section('content')
    <!-- CSRF Token -->
    <div class="div_input"><h4>Номер временного интервала:</h4> <input disabled="true"
                                                                      style="width: 50px; text-align: center"
                                                                      class="l form-control">из<input
                    disabled="true" style="width: 50px; text-align: center" class="inter form-control"></div>
    <div class="div_input"><h4>Номер решаемой задачи:</h4> <input disabled="true" style="width: 50px; text-align: center ;    margin-left: 42px;"
                                          class="k form-control">из<input disabled="true"
                                                                              style="width: 50px; /*text-align: center; padding: 5px*/"
                                                                              class="iter form-control"></div><br>
    <div id="first">
        <form role="form">
            <div class="panel panel-danger hidden">
                <div class="panel-body">Введенные значения n, m не входят в диапазон от 1 до 10</div>
            </div>
            <div class="form-group ">
                <div class="input-ko">
                    <label for="n">N: </label>
                    <input type="number" class="N form-control-next input-small" placeholder="Enter N" name="n" id="n" required
                           autofocus>
                </div>
                <div class="input-ko"><label for="m">M: </label>
                    <input type="number" class="M form-control-next input-small" placeholder="Enter M" name="m" id="m"
                           required> <br></div>
            </div>
            <div id="plan">
                <label for="">План Х:</label> <br>
                <table id="plan_table" class="table table-striped"></table>
            </div>
            <div id="matrix_a">
                <label for="">Матрица А:</label> <br>
                <table id="matrix_a_table" class="table table-striped"></table>
            </div>
            <button type="button" class="next btn btn-info input-lg">Готово</button>
        </form>
    </div>

@endsection