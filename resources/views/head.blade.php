@extends('layouts.app')
@section('content')
    <div id="calc_list">
        <h4>Выберите рассчет для вывода (отсортировано по дате):</h4>
        <ul>
            <?php if (isset($rolls)): ?>
            <?php foreach ($rolls as $item): ?>
            <form method="GET" action="actionHead/{{$item["id"]}}">
                <li style="width: 230px">
                    <button data-id="<?=$item["id"]?>" class="show_calc btn"><?=$item["name"]?> </button>
                </li>
                <br>
            </form>

            <?php endforeach; ?>
            <?php endif; ?>
            {{$rolls->links()}}
        </ul>
    </div>
@endsection