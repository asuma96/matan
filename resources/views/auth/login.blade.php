@extends('layouts.app')

@section('content')
    <h3 style="text-align: center;    margin-bottom: 65px;     font-size: 25px; font-weight: bold;">Приложение для генерации, сохранения и отображения показателей на основе введенных
        данных</h3>


            <div class="col-md-8 col-md-offset-2">
                    <div class="panel-body">
                        <div class="login-flex">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-2 control-label">Login</label>

                                <div class="col-md-10">
                                    <input id="login" type="text" class="form-control" name="login"
                                           value="{{ old('login') }}" required autofocus>

                                    @if ($errors->has('login'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-2 control-label">Password</label>

                                <div class="col-md-10">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary" style="    background-color: #20895e !important;    border-color: #196c4b !important;">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="login-text">
                            Все пользователи разделены на диспетчеров, руководителей и администраторов. Диспетчеры могут только
                            просматривать
                            данные, а администраторы только генерировать. admin:admin, disp:disp, head:head
                        </div>
                    </div>
                    </div>
                </div>

@endsection
