(function () {
  $('#calc_list li').on('click', function () {
    $('#calc_list').css('display', 'none')
  })
  $('.new').on('click', function () {
    delete localStorage["kAndl"]
    delete localStorage["arrays"]
    delete localStorage["mAndn"]
    delete localStorage["lastId"]
    delete localStorage["iters"]
  })
  $("#matrix_a label").empty();
  $("#plan label").empty();
  if (JSON.parse(localStorage.getItem("iters")) && JSON.parse(localStorage.getItem("iters")).inter) {
    $('.inter').val(JSON.parse(localStorage.getItem("iters")).inter)
  }
  if (JSON.parse(localStorage.getItem("iters")) && JSON.parse(localStorage.getItem("iters")).iter) {
    $('.iter').val(JSON.parse(localStorage.getItem("iters")).iter)
  }
  if (JSON.parse(localStorage.getItem("kAndl")) === null) {
    $('.k').val(1)
    $('.l').val(1)
  } else if (JSON.parse(localStorage.getItem("kAndl")).k === null) {
    $('.k').val(1)
  }
  else if (JSON.parse(localStorage.getItem("kAndl")).l === null) {
    $('.l').val(1)
  } else {
    $('.k').val(JSON.parse(localStorage.getItem("kAndl")).k)
    $('.l').val(JSON.parse(localStorage.getItem("kAndl")).l)
  }
  $('.start').on('click', function () {
    console.log($('.input-inter').val())
    var items = {"k": 1, "l": 1}
    localStorage.setItem("kAndl", JSON.stringify(items))
    var obj = {"inter": $('.input-inter').val(), "iter": $('.input-iter').val()}
    localStorage.setItem("iters", JSON.stringify(obj))
  })
  console.log('aloo', JSON.parse(localStorage.getItem("iters")))
  var edit_matrix = function () {
    $("#plan_table").empty();
    $("#matrix_a_table").empty();
    $(".panel-danger").addClass("hidden");

    var n = parseInt($("#n")[0].value);
    var m = parseInt($("#m")[0].value);

    if (n > 0 && m > 0 && n <= 10 && m <= 10) {
// НАЧАЛО ПЛАНА
      var table_content = "";
// шапка таблицы
      table_content += "<thead><tr>";
      for (var trhead_counter = 0; trhead_counter < n; trhead_counter++) {
        table_content += "<th>" + (trhead_counter + 1) + "</th>";
      }
      table_content += "</tr></thead>";
// конец шапки таблицы
      table_content += "<tr>";
      for (var j = 0; j < n; j++) {
        table_content += "<td><input type=\"number\" class=\"x_" + j + "\" name = \"plan_" + j + "\" ></td>";
      }
      table_content += "</tr>";
      table_content += "</tbody>";
      $("#plan_table").append(table_content);
// КОНЕЦ ПЛАНА

// НАЧАЛО МАТРИЦЫ А
      var table_content = "";
// шапка таблицы
      table_content += "<thead><tr>";
      for (var trhead_counter = 0; trhead_counter < n; trhead_counter++) {
        table_content += "<th>" + (trhead_counter + 1) + "</th>";
      }
      table_content += "</tr></thead>";
// конец шапки таблицы
      for (var i = 0; i < m; i++) {
        table_content += "<tr>";
        for (var j = 0; j < n; j++) {
          table_content += "<td><input type=\"number\" name = \"a_" + i + "_" + j + "\" class=\"a_" + i + "_" + j + "\" ></td>";
        }
        table_content += "</tr>";
      }
      table_content += "</tbody>";
      $("#matrix_a_table").append(table_content);
// КОНЕЦ МАТРИЦЫ А
    }
    else {
      if (!isNaN(n) && !isNaN(m)) $(".panel-danger").removeClass("hidden");
    }
  };

  $(".N").keyup(function (eventObject) {
    if (parseInt(JSON.parse(localStorage.getItem('kAndl')).k) === 1) {
      $('#plan').css('display', 'block')
      $('#matrix_a').css('display', 'block')
    } else {
      $('#matrix_a').css('display', 'block')
    }
    edit_matrix()
  });
  $(".M").keyup(function (eventObject) {
    if (parseInt(JSON.parse(localStorage.getItem('kAndl')).k) === 1) {
      $('#plan').css('display', 'block')
      $('#matrix_a').css('display', 'block')
    } else {
      $('#matrix_a').css('display', 'block')
    }
    edit_matrix()
  });
  var pin_ajax = function () {
    $("#working_form").submit(function (e) {
      $.ajax({
        type: "POST",
        url: "actionProcess",
        data: $("#working_form").serialize(),
        success: function (data) {
          $("#next").empty();
          data = JSON.parse(data);
          $("span#inter").empty();
          $("span#inter").append(data[0][1]);
          $("span#iter").empty();
          $("span#iter").append(data[1][1]);
          $("span#l").empty();
          $("span#l").append(data[0][0]);
          $("span#k").empty();
          $("span#k").append(data[1][0]);
          $("#next").append(data[2]);
          if (data[3] == true) {
            alert("Успешно! Вы будете перенаправлены.");
            location.href = "/";
          }
          e.preventDefault();
          pin_ajax();
        }
      });
      e.preventDefault();
    });
    $(".N").keyup(function (eventObject) {
      edit_matrix();
    });
    $(".M").keyup(function (eventObject) {
      edit_matrix();
    });
  }
  $('.next').on('click', function () {
    var m = $('.M').val()
    var n = $('.N').val()
    var iter = $('.iter').val()
    var inter = $('.inter').val()
    var k = $('.k').val()
    var l = $('.l').val()
    if (k === null) {
      k = 1
    }
    if (l === null) {
      l = 1
    }
    var items = {"k": $('.k').val(), "l": $('.l').val()}
    localStorage.setItem("kAndl", JSON.stringify(items))
    var a = new Array(m)
    for (var i = 0; i < m; i++) {
      a[i] = new Array(n)
    }
    var x = []
    for (var i = 0; i < m; i++) {
      for (var j = 0; j < n; j++) {
        a[i][j] = $(".a_" + i + "_" + j).val()
      }
      console.log('a', a)
    }
    for (var i = 0; i < n; i++) {
      x[i] = $(".x_" + i).val()
    }
    var params = {a: a, x: x, iter: iter, inter: inter, l: l - 1, k: k - 1, m: m, n: n}
    var M = []
    var BN = []
    var BV = []
    var DN = []
    var DV = []
    var N = []
    var DEL = []
    var AX = []
    var x_new = []
    var C = []
    var lastId = null
    if (JSON.parse(localStorage.getItem("mAndn")) && JSON.parse(localStorage.getItem("mAndn")).m_new) {
      params.n = JSON.parse(localStorage.getItem("mAndn")).n_new
      params.m = JSON.parse(localStorage.getItem("mAndn")).m_new
    }
    if (JSON.parse(localStorage.getItem("arrays")) && JSON.parse(localStorage.getItem("arrays")).M) {
      M = JSON.parse(localStorage.getItem("arrays")).M
      BN = JSON.parse(localStorage.getItem("arrays")).BN
      BV = JSON.parse(localStorage.getItem("arrays")).BV
      DN = JSON.parse(localStorage.getItem("arrays")).DN
      DV = JSON.parse(localStorage.getItem("arrays")).DV
      N = JSON.parse(localStorage.getItem("arrays")).N
      AX = JSON.parse(localStorage.getItem("arrays")).AX
      DEL = JSON.parse(localStorage.getItem("arrays")).DEL
      x_new = JSON.parse(localStorage.getItem("arrays")).x_new
      C = JSON.parse(localStorage.getItem("arrays")).C
      params = Object.assign(params, {M: M, BV: BV, DV: DV, BN: BN, DN: DN, N: N, AX: AX, DEL: DEL, x_new: x_new, C: C})
    }
    if (JSON.parse(localStorage.getItem("lastId")) && JSON.parse(localStorage.getItem("lastId")).lastId) {
      lastId = JSON.parse(localStorage.getItem("lastId")).lastId
      params = Object.assign(params, {lastId: lastId})
    }
// delete localStorage["kAndl"]
// delete localStorage["arrays"]
// delete localStorage["lastId"]
// delete localStorage["iters"]

    $.ajax({
      type: "POST",
      url: "actionProcess",
      data: params,
      success: function (data) {
        console.log('storege', JSON.parse(localStorage.getItem('iters')))
        console.log('bool', parseInt(data.k) === 1 && JSON.parse(localStorage.getItem('iters')) && parseInt(data.l) === (parseInt(JSON.parse(localStorage.getItem('iters')).inter) + 1))
        if (parseInt(data.k) === 1 && JSON.parse(localStorage.getItem('iters')) && parseInt(data.l) === (parseInt(JSON.parse(localStorage.getItem('iters')).inter) + 1)) {
          delete localStorage["kAndl"]
          delete localStorage["arrays"]
          delete localStorage["mAndn"]
          delete localStorage["lastId"]
          delete localStorage["iters"]
          window.location.href = "/last"
        }
        console.log('data', data)
        $('.k').val(data.k)
        $('.l').val(data.l)
        localStorage.setItem("kAndl", JSON.stringify({"k": data.k, "l": data.l}))
        localStorage.setItem("arrays", JSON.stringify({
          "M": data.M,
          "BN": data.BN,
          "BV": data.BV,
          "DN": data.DN,
          "DV": data.DV,
          "N": data.N,
          "AX": data.AX,
          "DEL": data.DEL,
          "x_new": data.x_new,
          "C": data.C
        }))
        if (data.lastId) {
          localStorage.setItem("lastId", JSON.stringify({"lastId": data.lastId}))
        }
        if (data.m_new && data.m_new !== null) {
          console.log('items', data)
          $('.M').val(data.m_new)
          $('.N').val(data.n_new)
          localStorage.setItem("mAndn", JSON.stringify({"m": data.m_new, "n": data.n_new}))
        }
        if (parseInt(data.l) === 1 && data.k > 1) {
          $('.N').prop('disabled', true)
          $('#plan').css('display', 'none')
          $('.M').val(null)
        } else if (data.l > 1 && parseInt(data.k) === 1) {
          $('.N').prop('disabled', true)
          $('.M').prop('disabled', true)
        } else if (data.l > 1 && data.k > 1) {
          $('.N').prop('disabled', true)
          $('.M').prop('disabled', true)
        }
        $('#plan').css('display', 'none')
        $('#matrix_a').css('display', 'none')
        if (parseInt(data.l) > 1 && parseInt(data.k) === 1) {
          $('#plan').css('display', 'block')
          edit_matrix()
          for (var i = 0; i < JSON.parse(localStorage.getItem('arrays')).x_new.length; i++) {
            console.log('val', $(".x_" + i).val())
            $(".x_" + i).val(JSON.parse(localStorage.getItem('arrays')).x_new[i])
            $(".x_" + i).prop('disabled', true)
          }
        }

// $("#first").hide();
// $("#next").show();
// data = JSON.parse(data);
//
// $("span#inter").empty();
// $("span#inter").append(data[0][1]);
// $("span#iter").empty();
// $("span#iter").append(data[1][1]);
//
// $("#next").empty();
// $("#next").append(data[2]);
        $("#first_form").remove();
        pin_ajax();
      },
      error: function (err) {
        console.log(err)
      }
    })
  })
  $('.show_calc').on('click', function () {
    console.log('1')
    var id = $(this).data('id')
    console.log(id)
    $.ajax({
      type: "GET",
      url: "example/" + id,
      success: function (data) {
        console.log('data', data)
// var res = JSON.parse(data);
        $("#m").html(data.m);
        $("#n").html(data.n);
        $("#c_cx").html(data.c_cx);
        $("#dn").html(data.dn);
        $("#dv").html(data.dv);
        $("#bn").html(data.bn);
        $("#bv").html(data.bv);
        $("#a").html(data.a);
        $("#x").html(data.x);
        $("#x_opt").html(data.x_opt);
        $("#b").html(data.b);
        $("#plan").html(data.plan_b);
          $("#head_plan").html(data.head_plan);

        $('.sound_data').on('click', function () {
          if (data.plan_b !== null) {
            $("#plan_b").html(data.plan_b)
          } else {
            $("#plan_b").html("Нет решения!")
          }
        })
        $(".roll_name").append(data.roll_name);
        $("#roll").removeClass('hidden');
      },
      error: function (err) {
        console.log(err)
      }
    })
  })
  $('.logout').on('click', function () {
    delete localStorage["kAndl"]
    delete localStorage["arrays"]
    delete localStorage["mAndn"]
    delete localStorage["lastId"]
    delete localStorage["iters"]
  })
})()