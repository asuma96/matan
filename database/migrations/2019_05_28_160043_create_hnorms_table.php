<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHnormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hnorms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hadditional_id')->nullable();
            $table->integer('hwsId')->nullable();
            $table->integer('hproductId')->nullable();
            $table->integer('hresId')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hnorms');
    }
}
