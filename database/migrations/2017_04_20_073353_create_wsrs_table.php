<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWsrsTable extends Migration
{
    public function up()
    {
        Schema::create('wsrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dateId')->nullable();
            $table->integer('wsId')->nullable();
            $table->integer('resId')->nullable();
            $table->integer('productId')->nullable();
            $table->integer('additional_id')->nullable();
            $table->float('res_amount')->nullable();
            $table->float('price')->nullable();
            $table->string('limit_down')->nullable();
            $table->string('limit_up')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wsrs');
    }
}
