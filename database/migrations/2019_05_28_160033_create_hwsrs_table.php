<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHwsrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hwsrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dateId')->nullable();
            $table->integer('hwsId')->nullable();
            $table->integer('hresId')->nullable();
            $table->integer('hproductId')->nullable();
            $table->integer('hadditional_id')->nullable();
            $table->float('hres_amount')->nullable();
            $table->float('price')->nullable();
            $table->string('limit_down')->nullable();
            $table->string('limit_up')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hwsrs');
    }
}
