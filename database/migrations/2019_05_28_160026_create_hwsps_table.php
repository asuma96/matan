<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHwspsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hwsps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dateId')->nullable();
            $table->integer('hwsId')->nullable();
            $table->integer('hproductId')->nullable();
            $table->integer('hadditional_id')->nullable();
            $table->string('limit_down')->nullable();
            $table->string('limit_up')->nullable();
            $table->float('plan')->nullable();
            $table->float('profit')->nullable();
            $table->float('plan_d')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hwsps');
    }
}
