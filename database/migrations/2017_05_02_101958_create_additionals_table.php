<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additionals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('c_cx')->nullable();
            $table->string('head_plan')->nullable();
            $table->string('a')->nullable();
            $table->string('x')->nullable();
            $table->string('b')->nullable();
            $table->string('c')->nullable();
            $table->string('bv')->nullable();
            $table->integer('m')->nullable();
            $table->integer('n')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('additionals');

    }
}
