<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWspsTable extends Migration
{
    public function up()
    {
        Schema::create('wsps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dateId')->nullable();
            $table->integer('wsId')->nullable();
            $table->integer('productId')->nullable();
            $table->integer('additional_id')->nullable();
            $table->string('limit_down')->nullable();
            $table->string('limit_up')->nullable();
            $table->float('plan')->nullable();
            $table->float('profit')->nullable();
            $table->float('plan_d')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wsps');
    }
}
