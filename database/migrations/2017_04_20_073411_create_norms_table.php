<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNormsTable extends Migration
{
    public function up()
    {
        Schema::create('norms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('additional_id')->nullable();
            $table->integer('wsId')->nullable();
            $table->integer('productId')->nullable();
            $table->integer('resId')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('norms');
    }
}
