<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $roles = [
            [
                'id' => '1',
                'created_at'=>'2017-05-19 19:15:56',
                'updated_at'=>'2017-05-19 19:15:56',
                'password'=>'$2y$10$XKuPkv8zwkWPo4KTGtXSJOCagLsDgOw0V56TyHgnkGpsoiqcvBxlC',
                'roleId'=>'1',
                'login'=>'admin'
            ],
            [
                'id' => '2',
                'created_at'=>'2017-05-19 19:15:56',
                'updated_at'=>'2017-05-19 19:15:56',
                'password'=>'$2a$04$0T2kzoCGUbKq5nki7E8dNuRy7ZPFhrXHSwEwSPg4ORvIrbBAtc2US',
                'roleId'=>'2',
                'login'=>'disp'
            ],
            [
                'id' => '3',
                'created_at'=>'2017-05-19 19:15:56',
                'updated_at'=>'2017-05-19 19:15:56',
                'password'=>'$2a$10$VPDsp3j/TZIZkBQzsTQowuTOYhf9/9qaYm2S8BnRcMH7ccyX8ZMym',
                'roleId'=>'3',
                'login'=>'head'
            ]

        ];

        DB::table('users')->insert($roles);
    }
}
