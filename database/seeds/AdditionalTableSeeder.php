<?php

use Illuminate\Database\Seeder;

class AdditionalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('additionals')->delete();

        $roles = [
            [
                'id' => '1',
                'created_at'=>'2017-11-28 01:32:17',
                'updated_at'=>'2017-11-28 01:32:17',
                'name'=>'out_1_1_date=11-28',
                'c_cx'=>'4 , 2, 2',
                'a'=>'1 1 1 1 <br>6 5 4 2 <br>4 6 10 12 <br>1 0 0 0 <br>0 1 0 0 <br>0 0 1 0 <br>0 0 0 1 <br>',
                'x'=>'1, 1, 1, 1',
                'b'=>'4, 0',
                'm'=>'7',
                'n'=>'4',
            ],

        ];

        DB::table('additionals')->insert($roles);
    }
}
