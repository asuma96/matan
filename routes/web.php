<?php
Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('actionProcess', 'SiteController@actionProcess');

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/disp', function () {
    return view('disp');
});
Route::get('/last', function () {
    return view('last');
});
Route::get('/home', 'HomeController@index');
Route::post('/next', 'SiteController@next');
Route::post('/actionProcess', 'SiteController@actionProcess');
Route::get('/disp', 'SiteController@actionOutput');
Route::get('/example/{id}', 'SiteController@actionGetcalcs');
Route::get('/orders', 'SiteController@getOrder'); //теститрование приложения java
Route::get('/head', function () {
    return view('head');
});
Route::get('/head', 'SiteController@actionOutputHead');
Route::get('/actionHead/{id}', 'SiteController@actionHead');