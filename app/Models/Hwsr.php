<?php

namespace App\Models;

use App\Models\Head;
use Illuminate\Database\Eloquent\Model;
use App\Models\Hproduct;

class Hwsr extends Model
{
    protected $table = 'hwsrs';
    protected $fillable = [
        'dateId',
        'hwsId',
        'hproductId',
        'hres_amount',
        'limit_down',
        'limit_app',
        'price',
        'hresId',
        'hadditional_id'
    ];

    public function dates()
    {
        return $this->belongsTo(Date::class, 'id', 'dateId');
    }

    public function heads()
    {
        return $this->belongsTo(Head::class, 'id', 'hwsId');
    }

    public function product()
    {
        return $this->belongsTo(Hproduct::class, 'id', 'hproductId');
    }

    public function resources()
    {
        return $this->belongsTo(Hresource::class, 'id', 'hresId');
    }
}
