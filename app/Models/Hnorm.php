<?php

namespace App\Models;

use App\Models\Head;
use Illuminate\Database\Eloquent\Model;
use App\Models\Hproduct;
use App\Models\Hresource;

class Hnorm extends Model
{
    protected $table = 'hnorms';
    protected $fillable = [
        'hwsId',
        'hproductId',
        'hresId',
        'value',
        'hadditional_id'
    ];
    public function heads()
    {
        return $this->belongsTo(Head::class,'id','hwsId');
    }
    public function products()
    {
        return $this->belongsTo(Hproduct::class,'id','hproductId');
    }
    public function resources()
    {
        return $this->belongsTo(Hresource::class,'id','hresId');
    }
}
