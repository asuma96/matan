<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Workshop;
use App\Models\Resource;
use Illuminate\Database\Eloquent\Model;
class Norm extends Model
{
    /**
     * @return array
     */
    
    protected $table = 'norms';
    protected $fillable = [
        'wsId',
        'productId',
        'resId',
        'value',
        'additional_id'
    ];
    public function workshops()
    {
        return $this->belongsTo(Workshop::class,'id','wsId');
    }
    public function products()
    {
        return $this->belongsTo(Product::class,'id','productId');
    }
    public function resources()
    {
        return $this->belongsTo(Resource::class,'id','resId');
    }
}
