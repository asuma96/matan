<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Workshop;
use App\Models\Date;
use App\Models\Product;

class Wsp extends Model
{
    protected $table = 'wsps';
    protected $fillable = [
        'dateId',
        'wsId',
        'productId',
        'limit_down',
        'limit_app',
        'plan',
        'profit',
        'plan_d',
        'additional_id'
    ];

    public function workshops()
    {
        return $this->belongsTo(Workshop::class, 'id', 'wsId');
    }

    public function dates()
    {
        return $this->belongsTo(Date::class, 'id', 'dateId');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'id', 'productId');
    }
    
    

}
