<?php

namespace App\Models;


use App\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = [
        'additional_id',
        'login',
        'password',
    ];

    protected $hidden = [

    ];
    public function role()
    {
        return $this->belongsTo(Role::class,'id','roleId');
    }

}


//class User extends Authenticatable
//{
//    use Notifiable,SearchableTrait;
//    /**
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
//    protected $fillable = [
//        'id', 'username', 'password','status'
//    ];
//
//    private static $users = [
//        1 => [
//            'id' => '100',
//            'username' => 'admin',
//            'password' => 'admin',
//            'status' => '3',
//        ],
//        2 => [
//            'id' => '101',
//            'username' => 'disp',
//            'password' => 'disp',
//            'status' => '2',
//        ],
//    ];
//    public static function login($username, $password){
//        foreach (self::$users as $user){
//            if ($user['username'] == $username && $user['password'] == $password){
//                $session = new Session();
//                $session->open();
//                $session['status'] = $user['status'];
//                return true;
//            }
//        }
//        return false;
//    }
//    public static function logoff(){
//        $session = new Session();
//        $session['status'] = NULL;
// */       $session->close();
//    }
//
//}