<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';
    protected $fillable = [
        'name',
        'measure'
    ];
    public function products()
    {
        return $this->belongsTo(Product::class,'id','productId');
    }
}
