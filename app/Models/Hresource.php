<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Hproduct;

class Hresource extends Model
{
    protected $table = 'hresources';
    protected $fillable = [
        'name',
        'measure'
    ];
    public function products()
    {
        return $this->belongsTo(Hproduct::class,'id','hproductId');
    }
}
