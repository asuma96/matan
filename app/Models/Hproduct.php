<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hproduct extends Model
{
    protected $table = 'hproducts';
    protected $fillable = [
        'name',
        'measure'
    ];
}
