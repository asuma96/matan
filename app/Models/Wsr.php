<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Date;
use App\Models\Workshop;
use App\Models\Product;
use App\Models\Resource;

class Wsr extends Model
{
    protected $table = 'wsrs';
    protected $fillable = [
        'dateId',
        'wsId',
        'productId',
        'res_amount',
        'limit_down',
        'limit_app',
        'price',
        'resId',
        'additional_id'
    ];

    public function dates()
    {
        return $this->belongsTo(Date::class, 'id', 'dateId');
    }

    public function workshops()
    {
        return $this->belongsTo(Workshop::class, 'id', 'wsId');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'id', 'productId');
    }

    public function resources()
    {
        return $this->belongsTo(Resource::class, 'id', 'resId');
    }
}
