<?php

namespace App\Models;

use App\Models\Head;
use Illuminate\Database\Eloquent\Model;
use App\Models\Hproduct;

class Hwsp extends Model
{
    protected $table = 'hwsps';
    protected $fillable = [
        'dateId',
        'hwsId',
        'hproductId',
        'limit_down',
        'limit_app',
        'plan',
        'profit',
        'plan_d',
        'hadditional_id'
    ];

    public function heads()
    {
        return $this->belongsTo(Head::class, 'id', 'hwsId');
    }

    public function dates()
    {
        return $this->belongsTo(Date::class, 'id', 'dateId');
    }

    public function products()
    {
        return $this->belongsTo(Hproduct::class, 'id', 'hproductId');
    }
}
