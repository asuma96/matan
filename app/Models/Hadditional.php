<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hadditional extends Model
{
    protected $table = 'hadditionals';
    protected $fillable = [
        'roll_id',
        'c_cx',
        'a',
        'x',
        'b',
        'm',
        'n',
        'name',
        'created_at',
        'head_plan',
        'bv'
    ];
}
