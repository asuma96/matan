<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Workshop;
use Illuminate\Http\Request;
use App\Models\Additional;
use App\Models\User;
use App\Models\Date;// подключение модели Dates
use App\Models\Norm;// подключение модели Norms
use App\Models\Roll;// подключение модели Rolls
use App\Models\Wsp;// подключение модели Wsps
use App\Models\Wsr;// подключение модели Wsrs
use App\Models\Head;//подключение модуля Head
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Hadditional;
use function Sodium\add;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function actionOutput()
    {
        // Вывод всех Rolls (отсортированных по дате) и проверка на то чтобы в меню Output входил только disp
        $rolls = Additional::query()->orderBy('created_at', 'desc')->paginate(10);
        return view('disp', compact('rolls'));
    }

    public function actionOutputHead()
    {
        // Вывод всех Rolls (отсортированных по дате) и проверка на то чтобы в меню Output входил только head
        $rolls = Hadditional::query()->orderBy('created_at', 'desc')->paginate(10);
        return view('head', compact('rolls'));
    }

    public function next(Request $request)
    {
        $data = $request->all();
        return view('next', compact('data'));
    }

    public function actionProcess(Request $request)
    {
        $M = array();
        $N = array();
        $C = array();
        $DN = array();
        $DV = array();
        $DEL = array();
        $BN = array();
        $BV = array();
        $Y = array();
        $AX = array();
        $CX = null;
        $lastId = null;
        $m_new = null;
        $n_new = null;
        $Matrix = array();
        $A = $request->a;
        $k = $request->k;
        $l = $request->l;
        $n = $request->n;
        $m = $request->m;
        $matrix_A = "";
        if ($l > 0 || $m != 0) {
            $C = $request->C;
        }
        if ($request->lastId !== null) {
            $Matrix = DB::table('additionals')->select('a')->where('id', $request->lastId)->first()->a;
            $m_new = DB::table('additionals')->select('m')->whereId($request->lastId)->first()->m;
            $n_new = DB::table('additionals')->select('n')->whereId($request->lastId)->first()->n;
            $Matr = explode('" ".<br>." " ', $Matrix, -1);
            for ($i = 0; $i < count($Matr); $i++) {
                $Matrica[$i] = explode(' ', $Matr[$i], -1);
            }
            $A = $Matrica;
        }

        if ($request->M !== null) {
            $M = $request->M;
            $DV = $request->DV;
            $DN = $request->DN;
            $BN = $request->BN;
            $BV = $request->BV;
            $AX = $request->AX;
            $N = $request->N;
        }
        for ($i = 0; $i < $m; $i++) {
            for ($j = 0; $j < $n; $j++) {
                $matrix_A .= $A[$i][$j] . " ";
            }
            $matrix_A .= "<br>";
        }
        $N[$k] = ((int)$k === 0) ? $n : $M[$k - 1];
        $M[$k] = $m;
        $new_date = new Date();
        $new_date->value = date('Y-m-d');
        $new_date->save();
        $additional = new Additional();

        $hadditional = new Hadditional();
        if (!DB::table('hadditionals')->select('id')->where('name', date("Y-m"))->first()) {
            $hadditional->name = date("Y-m");
            $hadditional->save();

        }
        $additional->name = "out_" . ($l + 1) . "_" . ($k + 1) . "_date=" . date("m-d");
        $additional->m = $m;
        $additional->n = $n;

        $additional_id = Additional::query()->orderBy('id', 'desc')->first()->id + 1;
        if ((int)$k === 0) {
            $X = $request->x;
        } else {
            $X = $request->x_new;
        }
        for ($j = 0; $j < $N[$k]; $j++) {
            if ((int)$k === 0) {
                if ($X[$j] < ($this->MinX($X, $N[$k]) + 0.35 * ($this->MaxX($X, $N[$k])))) {
                    $DN[$j] = $X[$j];
                    $DV[$j] = $this->MaxX($X, $N[$k]);
                    $DEL[$j] = $this->Randl($DN[$j] + 0.25 * ($DV[$j] - $DN[$j]), $DN[$j] + 0.45 * ($DV[$j] - $DN[$j]));
                }
                if ($X[$j] >= ($this->MinX($X, $N[$k]) + 0.35 * ($this->MaxX($X, $N[$k]))) && $X[$j] <= ($this->MinX($X, $N[$k]) + 0.65 * ($this->MaxX($X, $N[$k]) - $this->MinX($X, $N[$k])))) {
                    $DN[$j] = $this->MinX($X, $N[$k]);
                    $DV[$j] = $this->MaxX($X, $N[$k]);
                    $DEL[$j] = ($this->MaxX($X, $N[$k]) - $this->MinX($X, $N[$k])) / 2;
                }
                if ($X[$j] > ($this->MinX($X, $N[$k]) + 0.65 * ($this->MaxX($X, $N[$k]) - $this->MinX($X, $N[$k])))) {
                    $DN[$j] = $this->MinX($X, $N[$k]);
                    $DV[$j] = $X[$j];
                    $DEL[$j] = $this->Randl($DN[$j] + 0.55 * ($DV[$j] - $DN[$j]), $DN[$j] + 0.75 * ($DV[$j] - $DN[$j]));
                }
            } else {
                $DN[$j] = $BN[$j];
                $DV[$j] = $BV[$j];
                $X[$j] = $AX[$j];

                if ($X[$j] < ($DN[$j] + 0.35 * ($DV[$j] - $DN[$j]))) $DEL[$j] = $this->Randl($DN[$j] + 0.25 * ($DV[$j] - $DN[$j]), $DN[$j] + 0.45 * ($DV[$j] - $DN[$j]));
                if (($X[$j] >= ($DN[$j] + 0.35 * ($DV[$j] - $DN[$j]))) && ($X[$j] <= ($DN[$j] + 0.65 * $DV[$j] - $DN[$j]))) $DEL[$j] = ($DN[$j] + $DV[$j]) / 2;
                if ($X[$j] > ($DN[$j] + 0.65 * ($DV[$j] - $DN[$j]))) $DEL[$j] = $this->Randl($DN[$j] + 0.55 * ($DV[$j] - $DN[$j]), $DN[$j] + 0.75 * ($DV[$j] - $DN[$j]));
            }
        }
        if ((int)$l === 0) {
            for ($i = 0; $i < $M[$k]; $i++) {
                $AX[$i] = 0;
                for ($j = 0; $j < $n; $j++) {
                    $A = $request->a;
                    $AX[$i] = $AX[$i] + $A[$i][$j] * $X[$j];
                }
            }
        } else {
            for ($i = 0; $i < $M[$k]; $i++) {
                $AX[$i] = 0;
                for ($j = 0; $j < $N[$k]; $j++) {
                    try {
                        $AX[$i] = $AX[$i] + $A[$i][$j] * $X[$j];
                    } catch (\Exception $e) {
                    }
                }
            }
        }
        for ($i = 0; $i < $M[$k]; $i++) {
            if ($AX[$i] < ($this->MinX($AX, $M[$k]) + 0.35 * ($this->MaxX($AX, $M[$k]) - $this->MinX($AX, $M[$k])))) {
                $BN[$i] = $AX[$i];
                $BV[$i] = $this->MaxX($AX, $M[$k]);
                $Y[$i] = $this->Randl($BN[$i] + 0.25 * ($BV[$i] - $BN[$i]), $BN[$i] + 0.45 * ($BV[$i] - $BN[$i]));
            }
            if (($AX[$i] >= ($this->MinX($AX, $M[$k]) + 0.35 * ($this->MaxX($AX, $M[$k]) - $this->MinX($AX, $M[$k])))) && ($AX[$i] <= ($this->MinX($AX, $M[$k]) + 0.65 * ($this->MaxX($AX, $M[$k]) - $this->MinX($AX, $M[$k]))))) {
                $BN[$i] = $this->MinX($AX, $M[$k]);
                $BV[$i] = $AX[$i];
                $Y[$i] = ($this->MaxX($AX, $M[$k]) - $this->MinX($AX, $M[$k])) / 2;
            }
            if ($AX[$i] > ($this->MinX($AX, $M[$k]) + 0.65 * ($this->MaxX($AX, $M[$k]) - $this->MinX($AX, $M[$k])))) {
                $BN[$i] = $this->MinX($AX, $M[$k]);
                $BV[$i] = $AX[$i];
                $Y[$i] = $this->Randl($BN[$i] + 0.55 * ($BV[$i] - $BN[$i]), $BN[$i] + 0.75 * ($BV[$i] - $BN[$i]));
            }
        }
        $CX = 0;
        for ($j = 0; $j < $N[$k]; $j++) {
            try {
                $C[$j] = $DEL[$j];
                for ($i = 0; $i < $m; $i++) $C[$j] = $C[$j] + $A[$k][$i][$j] * $Y[$i];
                $CX = $CX + $C[$j] * $X[$j];
            } catch (\Exception $e) {
            }
        }

        $wsp = new Wsp();
        $wsr = new Wsr();
        $norm = new Norm();

        $additional->c_cx .= $CX . " , ";
        for ($j = 0; $j < $N[$k]; $j++) {
            try {
                $additional->c_cx .= $C[$j] . ", ";
            } catch (\Exception $e) {
            }
        }
        $additional->c_cx = substr($additional->c_cx, 0, -2);
        for ($j = 0; $j < $N[$k]; $j++) {
            $wsp->limit_down .= $DN[$j] . ", ";
        }
        $wsp->limit_down = substr($wsp->limit_down, 0, -2);
        for ($j = 0; $j < $N[$k]; $j++) {
            $wsp->limit_up .= $DV[$j] . ", ";
        }
        $wsp->limit_up = substr($wsp->limit_up, 0, -2);

        for ($j = 0; $j < $M[$k]; $j++) {
            $wsr->limit_down .= $BN[$j] . ", ";
        }
        $wsr->limit_down = substr($wsr->limit_down, 0, -2);
        for ($j = 0; $j < $M[$k]; $j++) {
            $wsr->limit_up .= $BV[$j] . ", ";
        }
        $wsr->limit_up = substr($wsr->limit_up, 0, -2);
        for ($i = 0; $i < $M[$k]; $i++) {
            for ($j = 0; $j < $n; $j++) {
                try {
                    $additional->a = $matrix_A;
                } catch (\Exception $e) {
                }
            }
            $additional->a = $matrix_A;
        }
        for ($j = 0; $j < $N[$k]; $j++) {
            $additional->x .= $DV[$j] . ", ";
        }
        $additional->x = substr($additional->x, 0, -2);
        for ($j = 0; $j < $N[$k]; $j++) {
            $norm->value .= $X[$j] . ", ";
        }
        $norm->value = substr($norm->value, 0, -2);

        for ($j = 0; $j < $M[$k]; $j++) {
        }

        $wsp->additional_id = $additional_id;
        $wsr->additional_id = $additional_id;
        $norm->additional_id = $additional_id;
        $conditions = array();
        for ($i = 0; $i < count($A); $i++) {
            $conditions[$i] = ["coefficients" => $A[$i], "conditionType" => '1', "result" => $BV[$i]];
        }
        $additional->bv = serialize($BV);
        $new_plan = $this->makeRequest($plan = [
            "conditions" => $conditions,
            "linearProgram" => $C
        ]);
        $additional->c = serialize($C);
        if ($new_plan && $new_plan !== 0) {
            for ($j = 0; $j < count($new_plan); $j++) {
                $additional->b .= $new_plan[$j] . ", ";
            }
        } else {
            $new_plan = $this->getOrder($n);
            for ($j = 0; $j < count($new_plan); $j++) {
                $additional->b .= $new_plan[$j] . ", ";
            }
        }
        $additional->b = substr(substr($additional->b, 0, -1), 0, -1);
        $additional->head_plan = serialize($A);

        $additional->save();
        $wsp->save();
        $wsr->save();
        $norm->save();

        $k++;
        if ($l > 0 || ($k) == $request->iter) {
            $lastId = DB::table('additionals')->select('id')->orderBy('id', 'desc')->first()->id - (int)$request->inter + 1;
            $m_new = DB::table('additionals')->select('m')->whereId($lastId)->first()->m;
            $n_new = DB::table('additionals')->select('n')->whereId($lastId)->first()->n;

            if (($k) == $request->iter) {
                $k = 0;
                $l = (int)$l + 1;
            }
        }
        Additional::query()->where('id', 1)->delete();


        return [
            'lastId' => $lastId,
            'iter' => $request->iter,
            'inter' => $request->inter,
            'M' => $M,
            'BN' => $BN,
            'BV' => $BV,
            'DN' => $DN,
            'DV' => $DV,
            'AX' => $AX,
            'DEL' => $DEL,
            'x_new' => $new_plan,
            'N' => $N,
            'l' => $l + 1,
            'k' => $k + 1,
            'm_new' => $m_new,
            'n_new' => $n_new,
            'C' => $C,
        ];


    }

    private function MinX($val = array(), $size)
    {
        $min_val = $val[0];
        for ($i = 0; $i < $size - 1; $i++) {
            if ($val[$i] <= $min_val) $min_val = $val[$i];
        }
        return $min_val;
    }

    private function MaxX($val = array(), $size)
    {
        $max_val = $val[0];
        for ($i = 0; $i < $size - 1; $i++) {
            if ($val[$i] >= $max_val) $max_val = $val[$i];
        }
        return $max_val;
    }

    private function Randl($rfrom, $rto)
    {
        $d = rand() * ($rto - $rfrom) + $rfrom;
        $i = round($d * 10);
        return $i / 10;
    }

    public function actionGetcalcs(int $id) //вывод данных диспетчеру
    {
        $result = array();
        $add = Additional::query()->whereId($id)->first();
        $norm = Norm::query()->where('additional_id', $id)->first();
        $wsr = Wsr::query()->where('additional_id', $id)->first();
        $wsp = Wsp::query()->where('additional_id', $id)->first();
        $result['roll_name'] = $add['name'];
        $result['n'] = $add["n"];
        $result['m'] = $add["m"];
        $result['plan_b'] = $add["b"];
        $result['c_cx'] = $add['c_cx'];
        $result['dn'] = $wsp['limit_down'];
        $result['dv'] = $wsp['limit_up'];
        $result['bn'] = $wsr['limit_down'];
        $result['bv'] = $wsr['limit_up'];
        $result['a'] = $add['a'];
        $result['x'] = $add['x'];
        $result['x_opt'] = $norm['value'];
        $result['b'] = $add['b'];
        return $result;
    }

    public function getOrder($n)  //тестовый метод
    {


        if ($n == 2) {
            return $this->makeRequest(
                ["conditions" =>
                    [["coefficients" => [2.0, 2.0], "conditionType" => 1, "result" => 4.0],
                        ["coefficients" => [2.0, 2.0], "conditionType" => 1, "result" => 8.0],
                        ["coefficients" => [2.0, 2.0], "conditionType" => 1, "result" => 6.0],],
                    "linearProgram" => [2.0, 2.0]]);
        } elseif
        ($n == 3) {
            return $this->makeRequest(
                [
                    "conditions" =>
                        [
                            ["coefficients" => [3.0, 3.0, 3.0], "conditionType" => 1, "result" => 6.0],
                            ["coefficients" => [3.0, 3.0, 3.0], "conditionType" => 1, "result" => 10.0],
                            ["coefficients" => [2.0, 2.0, 2.0], "conditionType" => 1, "result" => 10.0],

                        ],
                    "linearProgram" => [2.0, 2.0, 2.0]
                ]);
        } else {
            return $this->makeRequest(
                [
                    "conditions" =>
                        [
                            ["coefficients" => [3.0, 3.0, 3.0, 2.0], "conditionType" => 1, "result" => 6.0],
                            ["coefficients" => [3.0, 3.0, 3.0, 2.0], "conditionType" => 1, "result" => 11, 0],
                            ["coefficients" => [2.0, 2.0, 2.0, 2.0], "conditionType" => 1, "result" => 10.0],
                            ["coefficients" => [2.0, 2.0, 2.0, 2.0], "conditionType" => 1, "result" => 12.0],
                        ],
                    "linearProgram" => [2.0, 2.0, 2.0, 2.0]
                ]);
        }

    }


    private function makeRequest($data) //связь с java приложением
    {

        //dd(json_encode($data)); //check
        $ch = curl_init('http://localhost:8080/equality-calc-api/result');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $result = curl_exec($ch);
        curl_close($ch);
        //dd(json_decode($result));
        return json_decode($result);
    }

    public function actionHead($id)
    {
        $massiv = array();
        $hadditional = Hadditional::query()->whereId($id)->first();
        $month = substr($hadditional['name'], 5);
        $year = substr($hadditional['name'], 0, 4);
        $additional = Additional::query()->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)->orderBy('created_at', 'asc')
            ->get();
        $n = $additional[0]->n;

        for ($i = 0; $i < count($additional); $i++) {
            if ($n == $additional[$i]->m) {
                $mas[$i] = unserialize($additional[$i]->head_plan);

            }
        }
        for ($i = 0; $i < count($additional); $i++) {
            $head_c[$i] = unserialize($additional[$i]->c);
        }
        for ($i = 0; $i < count($additional); $i++) {
            $head_bv[$i] = unserialize($additional[$i]->bv);
        }
        $r = 2;
        foreach ($additional as $add) {
            for ($a = 0; $a < count($additional); $a++) {
                for ($t = 0; $t < $add->n; $t++) {
                    if ($add->n > 2) {
                        $r = $add->n;
                    }
                }
            }
        }

        for ($e = 0; $e < $additional[0]->n; $e++) {
            for ($q = 0; $q < $r; $q++) {
                $head_matrix[$e][$q] = 0;
            }
        }
        for ($y = 0; $y < $additional[0]->n; $y++) {
            for ($x = 0; $x < $additional[0]->m; $x++) {
                $head_matrix[$x][$y] += $mas[0][$x][$y];
            }

        }
        $C = array();
        for ($t = 0; $t < count($head_c); $t++) {
            if (count($head_c[$t]) > 2) {
                $r = count($head_c[$t]);
            }
        }

        //foreach ($head_c as $head) {
//
        //    for ($i = 0; $i < count($head); $i++) {
        //        $C[$i] = ($C[$i] + $head[$i]);
        //    }
        //}
        //$mas = array_splice($mas, 1);
        //dd($C);
        foreach ($mas as $massiv) {
            $c = count($massiv[0]);
            $p = count($massiv);
            for ($j = 0; $j < $c; $j++) {
                for ($k = 0; $k < $p; $k++) {
                    $head_matrix[$k][$j] *= $massiv[$k][$j];
                }
            }
        }
        //dd($head_matrix);

        $end_matrix=array();
        for ($e = 0; $e < $additional[0]->n; $e++) {
            for ($q = 0; $q < $r; $q++) {
                $F_matrix[$e][$q] = 0;
            }
        }
       // dd($head_c, $mas);
        foreach ($mas as $massiv) {
            foreach ($head_c as $value_c) {
                //dd($massiv, $value_c);

                $c = count($massiv[0]);
                $p = count($massiv);
                //for ($q = 0; $q < $r; $q++) {
                //    $C[$q] = 0;
                //}
                for ($j = 0; $j < $c; $j++) {
                    for ($k = 0; $k < $p; $k++) {
                        $F_matrix[$k][$j] += $value_c[$k] * $massiv[$k][$j];
                    }
                }


            }
        }
        $end_matrix += $F_matrix;

       //foreach ($mas as $massiv) { //если мы перемножаем общее число C на каждую матрицу
       //    //    dd($massiv);
       //    //    $l=count($massiv);
       //    //    $v=count($massiv[0]);
       //    //    for ($j = 0; $j < $v; $j++) {
       //    //        for ($k = 0; $k < $l; $k++) {
       //    //            $C[$j] *= $massiv[$k][$j];
       //    //        }
       //    //    }
       //    //}
       //    //dd($C);

       //    //если мы перемножаем сумму C на произведение массивов
       //
       //    $l = count($head_matrix);
       //    $v = count($head_matrix[0]);
       //    for ($j = 0; $j < $v; $j++) {
       //        for ($k = 0; $k < $l; $k++) {
       //            $C[$j] *= $head_matrix[$k][$j];
       //        }
       //    }
       //}

       //dd($head_matrix, $C);

        foreach ($head_bv as $bv) {
            $r = count($bv);
            $p = count($head_bv);
            for ($q = 0; $q < $r; $q++) {
                $BV[$q] = 0;
            }
            for ($k = 0; $k < $p; $k++) {
                for ($i = 0; $i < $r; $i++) {

                    $BV[$i] += $head_bv[$k][$i];
                }
            }
        }

        //dd($BV);
        $conditions = array();
        for ($i = 0; $i < count($head_matrix); $i++) {
            $conditions[$i] = ["coefficients" => $head_matrix[$i], "conditionType" => '1', "result" => $BV[$i]];
        }
        $plan = [
            "conditions" => $conditions,
            "linearProgram" => $end_matrix
        ];
        
        $head_plan = $this->makeRequest($plan);
        dd($head_plan);

    }
}